Search
======

Ferris provides integration into App Engine's `search api <https://developers.google.com/appengine/docs/python/search/>`_ by providing utilities for indexing :doc:`models <models>` and for retrieving model instances from results.


Indexing Models
---------------

.. module:: ferris.behaviors.searchable

The :class:`Searchable` behavior will automatically add entities to the search index when saved. This is a wrapper around :func:`~ferris.core.search.index_entity`::

    from ferris import Model
    from ferris.behaviors import searchable

    class Post(Model):
        class Meta:
            behaviors = (searchable.Searchable,)

        title = ndb.StringProperty()
        context = ndb.TextProperty()

.. autoclass:: Searchable

This behavior can be configured using the meta class::

    class Meta:
        behaviors = (searchable.Searchable,)
        search_index = 'auto_ix_Post'
        search_exclude = ('thumbnail', 'likes')

.. attribute:: SearchableMeta.search_index

    Which search index to add the entity's data to. By default this is ``auto_ix_[Model]``. You can set it to a list or tuple to add the entity data to muliple indexes

.. attribute:: SearchableMeta.search_fields

    A list or tuple of field names to use when indexing. If not specified, all fields will be used.

.. attribute:: SearchableMeta.search_exclude

    A list or tuple of field names to exclude when indexing.

.. attribute:: SearchableMeta.search_callback

    A callback passed to :func:`~ferris.core.search.index_entity`.


Performing Searches
-------------------

.. module:: ferris.components.search

The most common use case for search is to present search results to the user. The :class:`Search` component provides a wrapper around :func:`~ferris.core.search.search` to make querying and index and transforming the results into ndb entities easy::

    from ferris import Controller
    from ferris.components.search import Search

    class Posts(Controller):
        class Meta:
            components = (Search,)

        def list(self):
            return self.components.search()

This component plays well with :doc:`scaffolding <scaffolding>`, :mod:`pagination <ferris.components.pagination>`, and :doc:`messages <messages>`.

.. autoclass:: Search

.. automethod:: Search.search()

When searching the component:

    * Determines the index to search.
    * If pagination is used, gets the cursor.
    * Performs the search using :func:`~ferris.core.search.search`.
    * Transforms the results into ndb entities.
    * Sets pagination data
    * Sets ``search_query`` and ``search_results`` in the view context.

You can pass in configuration to component when calling :meth:`~Search.search`::

    @route
    def list(self):
        self.components.search(
            'global',
            query=self.request.params.get('query')
        )


Advanced Usage
--------------

.. module:: ferris.core.search

The :mod:`search` module provides the functionality that's wrapped by the searchable behavior and the search component. You can use this directly to have greater control over how things are indexed or search. You can also use :func:`search` directly to search outside of a controller context.

.. autofunction:: index_entity

.. autofunction:: unindex_entity

.. autofunction:: search

.. autofunction:: join_query

.. autofunction:: transform_to_entities
