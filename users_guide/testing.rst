Testing
=======

.. module:: ferris.tests.lib

Ferris provides utilities to test your application using `nose <https://nose.readthedocs.org/en/latest/>`_ and `webtest <http://webtest.pythonpaste.org/en/latest/>`_. These utilites help you focus on testing your application instead of dealing with mocking App Engine.


Installing the nose plugin
--------------------------

`FerrisNose <https://pypi.python.org/pypi/FerrisNose>`_ is a nose plugin that allows you to run isolated Google App Engine tests. Install it via pip::

    pip install ferrisnose


.. note:: You may have to run this with sudo on linux and mac.

.. note:: You may need to specify the --pre flag to pip.


Running tests
-------------

To run tests just use nose::

    nosetests --with-ferris /app/tests

.. warning:: be sure to specify a path or nose will try to discover all tests which could fail in weird ways.


Writing tests for models
------------------------

Models and other parts of the application that don't involve HTTP/WSGI (such as services) can be tested using :class:`WithTestBed`.

.. autoclass:: WithTestBed

Here is a trivial example::

    from app.models.cat import Cat
    from ferris.tests.lib import WithTestBed

    class CatTest(WithTestBed):
        def test_herding(self):
            Cat(name="Pickles").put()
            Cat(name="Mr. Sparkles").put()

            assert Cat.query().count() == 2


Writing tests for controllers
-----------------------------

Controller, components, and templates usually have to be tested within a full WSGI environment. This is where :class:`FerrisTestCase`` comes in handy:

.. autoclass:: FerrisTestCase

    Provides all of the functionality of :class:`WithTestBed` as well as a blank webtest instance available at ``self.testapp``.

To add controllers to the testapp use :meth:`addController`.

.. automethod:: FerrisTestCase.addController

Here's an example of writing a test case for a single controller::

    from app.controllers.cats import Cats
    from ferris.tests.lib import FerrisTestCase

    class CatsTest(FerrisTestCase):
        def test_herding_method(self):
            self.addController(Cats)

            r = self.testapp.get('/cats')

            assert "Pickles" in r


Writing tests for the entire application
----------------------------------------
    
Sometimes you want the entire application to be up and running. In this instance you can use :class:`AppTestCase`. Like :class:`FerrisTestCase` it exposes a webtest instance via ``self.testapp`` but it automatically includes all controllers and plugins defined in your app. 

.. autoclass:: AppTestCase

This allows you to make requests to your application just as you would if the application were running::

    from ferris.tests.lib import AppTestCase

    class CatsTest(AppTestCase):
        def test_herding(self):
            Cat(name="Pickles").put()
            Cat(name="Mr. Sparkles").put()

            r = self.testapp.get('/cats')

            assert "Pickles" in r
            assert "Mr. Sparkles" in r
