Components
==========

Components are reusable pieces of functionality that can be invoked via a controller or react to controller events. This are a great way to re-use code between controllers, such as with pagination, searching, emailing, etc.


Using Components
----------------

Import the components and include them in the ``Meta.components`` property on your controller, like so::

    from ferris.components.pagination import Pagination
    from ferris.components.search import Search

    class Documents(Controller):
        class Meta:
            components = (Pagination, Search)

Inside of your actions, you can access the component instances using ``self.components``::

    def list(self):
        self.components.pagination()


Creating Components
-------------------

You can of course create your own components. Here's a very simple component that listens for the startup event and logs::

    class Logger(object):
        def __init__(self, controller):
            self.controller = controller
            self.controller.events.after_start += self.on_after_startup

        def on_after_startup(self, controller):
            logging.info("Hello!")



Built-in Components
-------------------

.. module:: ferris.components

The :mod:`ferris.components` module provides a few built-in components. These can be used directly, customized, or used as guidelines when building your own.


Cache
~~~~~

.. module:: ferris.components.cache

The Cache component provides utilities for doing client-side (edge) caching. This is not to be confused with memcache which is server-side caching.

.. autoclass:: Cache

Usage is pretty easy. Include the cache component and use the ``set_cache`` decorator.

.. autofunction:: set_cache

For example::

    from ferris import Controller, route
    from ferris.components.cache import Cache, set_cache

    class Posts(Controller):
        class Meta:
            components = (Cache)

        @route
        @set_cache('public', 60*60)
        def list_all(self):
            ...

        @route
        @set_cache('private', 60*60)
        def list_mine(self):
            ...

OAuth
~~~~~

The OAuth component is documented in :doc:`oauth2`.


Pagination
~~~~~~~~~~

.. module:: ferris.components.pagination

.. autoclass:: ferris.components.pagination.Pagination

.. automethod:: ferris.components.pagination.Pagination.paginate

For example of using this, see :doc:`../tutorial/7_extras`

You can also use the pagination component to provide pagination for custom datasets such as external apis or CloudSQL. The pagination component can automatically keep track of previous cursors to provide backwards pagination. For example::

    cursor, limit = self.components.pagination.get_pagination_params()

    data, next_cursor = get_custom_data(cursor, limit)

    self.components.pagination.set_pagination_info(
        current_cursor=cursor,
        next_cursor=next_cursor,
        limit=limit,
        count=len(data)
    )

.. automethod:: ferris.components.pagination.Pagination.get_pagination_params

.. automethod:: ferris.components.pagination.Pagination.set_pagination_info

.. automethod:: ferris.components.pagination.Pagination.get_pagination_info


.. _pagination_macros:

There is also a helpful macro to print simple forward and backwards pagination controls::

    {% import "macros/pagination.html" as p %}

    {{p.next_page_link()}}


.. method:: PaginationMacros.next_page_link()

    Generates bootstrap style forward and backwards pagination arrows.


Upload
~~~~~~

.. module:: ferris.components.upload

The :class:`Upload` component can take the guesswork out of using the  `Blobstore API <https://developers.google.com/appengine/docs/python/blobstore/>`_ to upload binary files.

.. autoclass:: ferris.components.upload.Upload

This component is designed to work instantly with with scaffolding and forms. Almost no configuration is needed::

    from ferris import Model, ndb, Controller, scaffold
    from ferris.components.upload import Upload

    class Picture(Model):
        file = ndb.BlobKeyProperty()

    class Pictures(Controller):
        class Meta:
            components = (scaffold.Scaffolding, Upload)

        add = scaffold.add
        edit = scaffold.edit
        list = scaffold.list
        view = scaffold.view
        delete = scaffold.delete

However, there are instances where you need more direct access. This is possible as well. Upload happens in two phases. First, you have to generate an upload url and provide that to the client. The client then uploads files to that URL. When the upload is successful the special upload handler will redirect back to your action with the blob data. Here's an example of that flow for a JSON/REST API::

    
    from ferris import Controller, route
    from ferris.components.upload import Upload

    class Upload(Controller):
        class Meta:
            components = (Upload,)

        @route
        def url(self):
            return self.components.upload.generate_upload_url(action='complete')

        @route
        def complete(self):
            uploads = self.components.upload.get_uploads()
            for blobinfo in uploads:
                logging.info(blobinfo.filename)

            return 200


.. automethod:: ferris.components.upload.Upload.generate_upload_url

.. automethod:: ferris.components.upload.Upload.get_uploads


Search
~~~~~~

The search component is documented in :mod:`search <ferris.components.search>`.
